
/* top search form
================================================== */
jQOld(document).ready(function() {
	jQOld('.search-wrapper.top .submit').click(function(e){
		if(jQOld('.search-wrapper.top').width()==45) {
			e.preventDefault();
			var a = jQOld('#main-nav').width();
			var b = jQOld('#main-nav > li').size();
			var e = jQOld('#main-nav > li > a > span.sf-sub-indicator').size();
			a = a-740;
			if (a>0) {
			b = a/b;
			var c = Math.round(Math.round(b)/2);
			c = e+7-c;
			var d = 0;
			}
		jQOld(this).parent().addClass("open");
		jQOld(this).parent().animate({width:"218px"},300);
		jQOld(this).siblings('input').val('').focus().select();
		jQOld(this).parent().parent().siblings('#main-nav').find('> li > a > .left > .right > .middle').animate({'padding-left':c,'padding-right':c},300);
		jQOld(this).parent().parent().siblings('#main-nav').find('> li > a > .sf-sub-indicator').animate({'width':d},300);
		return false;
		}
		if(jQOld(this).siblings('.text').val()=='') {
		return false;
		}
	});
	jQOld(".search-wrapper.top").click(function(){
	   if (event.stopPropagation){
		   event.stopPropagation();
	   }
	   else if(window.event){
		  window.event.cancelBubble=true;
	   }
	});
	jQOld("html").click(function(){
		if (jQOld('.search-wrapper.top').width() == 218) {
		jQOld(this).find('.search-wrapper.top').removeClass("open");
		jQOld(this).find('.search-wrapper.top').animate({width:'45px'},300);
		jQOld(this).find('#main-nav > li > a > .left > .right > .middle').animate({'padding-left':'10px','padding-right':'10px'},300);
		jQOld(this).find('#main-nav > li > a.sf-with-ul > .left > .right > .middle').animate({'padding-right':'1.25em'},300);
		jQOld(this).find('#main-nav > li > a > .sf-sub-indicator').animate({width:'9px'},300);
		}
	});
});