<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/consultas/empresa", name="company")
     * 
     */
    public function companyAction()
    {
        $slides = $this->getDoctrine()->getRepository('AppBundle:SlideCompany')->findInOrder();
        return $this->render('default/homepage.html.twig', array('slides' => $slides));
    }
    /**
     * 
     * @Route("/art/empresa", name="company_art")
     */
    public function companyARTAction()
    {
    	$slides = $this->getDoctrine()->getRepository('AppBundle:SlideCompany')->findInOrder();
    	return $this->render('default/homepageART.html.twig', array('slides' => $slides));
    }

    /**
     * @Route("/consultas/preguntas-frecuentes", name="faq")
     */
    public function faqAction()
    {
        return $this->render('default/faq.html.twig');
    }

    /**
     * @Route("/art/preguntas-frecuentes", name="faq_art")
     */
    public function faqARTAction()
    {
        return $this->render('default/faq.html.twig');
    }

    /**
     * @Route("/consultas/servicios", name="services")
     */
    public function servicesAction()
    {
        return $this->render('default/services.html.twig');
    }

    /**
     * @Route("/consultas/novedades-del-dia", name="todayNews")
     */
    public function todayNewsAction(Request $request)
    {
        $user = $this->getUser();
        $repository = $this->get('doctrine')->getRepository('OldBundle:iNOVEDADES', 'old');

        $news = $repository->findBy(
            array('codigoe' => $user->getCodigopal(), 'fecha' => new \DateTime('today')),
            array('fecha' => 'DESC')
        );

        if (empty($news)) {
            $new = $repository->findOneBy(
                array('codigoe' => $user->getCodigopal()),
                array('fecha' => 'DESC')
            );
            $news = $repository->findBy(
                array('codigoe' => $user->getCodigopal(), 'fecha' => $new->getFecha()),
                array('fecha' => 'DESC')
            );
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('default/todayNews.html.twig', array('news' => $pagination));
    }

    /**
     * @Route("/consultas/novedades-del-dia/imprimir", name="todayNewsPrint")
     */
    public function todayNewsActionPrint()
    {
        $user = $this->getUser();
        $repository = $this->get('doctrine')->getRepository('OldBundle:iNOVEDADES', 'old');

        $news = $repository->findBy(
            array('codigoe' => $user->getCodigopal(), 'fecha' => new \DateTime('today')),
            array('fecha' => 'DESC')
        );

        if (empty($news)) {
            $new = $repository->findOneBy(
                array('codigoe' => $user->getCodigopal()),
                array('fecha' => 'DESC')
            );
            $news = $repository->findBy(
                array('codigoe' => $user->getCodigopal(), 'fecha' => $new->getFecha()),
                array('fecha' => 'DESC')
            );
        }

        return $this->render('default/todayNewsPrint.html.twig', array('news' => $news));
    }

    /**
     * @Route("/consultas/novedades-del-mes", name="monthNews")
     */
    public function monthNewsAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo AND a.fecha >= :date ORDER BY a.fecha DESC");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('date', new \DateTime('midnight first day of this month'));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('default/monthNews.html.twig', array('news' => $pagination));
    }

    /**
     * @Route("/consultas/novedades-del-mes/imprimir", name="monthNewsPrint")
     */
    public function monthNewsActionPrint()
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo AND a.fecha >= :date ORDER BY a.fecha DESC");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('date', new \DateTime('midnight first day of this month'));

        return $this->render('default/monthNewsPrint.html.twig', array('news' => $query->getResult()));
    }

    /**
     * @Route("/consultas/novedades-del-anio", name="yearNews")
     */
    public function yearNewsAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo AND a.fecha >= :date ORDER BY a.fecha DESC");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('date', new \DateTime('first day of January '.date('Y')));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('default/yearNews.html.twig', array('news' => $pagination));
    }

    /**
     * @Route("/consultas/novedades-del-anio/imprimir", name="yearNewsPrint")
     */
    public function yearNewsActionPrint()
    {
        $user  = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo AND a.fecha >= :date ORDER BY a.fecha DESC");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('date', new \DateTime('first day of January '.date('Y')));

        return $this->render('default/yearNewsPrint.html.twig', array('news' => $query->getResult()));
    }

    /**
     * @Route("/consultas/estudios-medicos", name="medicalStudies")
     */
    public function medicalStudiesAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');

        if ($request->isMethod('POST')) {
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iESTUDIOS a where a.codigo = :codigo AND a.paciente = :paciente");
            $query->setParameter('codigo', $user->getCodigopal());
            $query->setParameter('paciente', $request->get('patient'));
            $patient = $query->getResult();

            return $this->render('default/medicalStudiesUser.html.twig', array('patient' => array_shift($patient)));
        } else {
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iESTUDIOS a where a.codigo = :codigo GROUP BY a.paciente ORDER BY a.paciente");
            $query->setParameter('codigo', $user->getCodigopal());
            $patients = $query->getResult();

            return $this->render('default/medicalStudies.html.twig', array('patients' => $patients));
        }
    }

    /**
     * @Route("/consultas/historia-clinica", name="clinicStory")
     */
    public function clinicStoryAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');

        if ($request->isMethod('POST')) {
        	
        	//LOG_ALERT("clinicStoryAction");
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo AND a.codigop = :paciente");
            $query->setParameter('codigo', $user->getCodigopal());
            $query->setParameter('paciente', $request->get('patient'));
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $request->query->get('page', 1)/*page number*/,
                10/*limit per page*/
            );

            return $this->render('default/clinicStoryUser.html.twig', array('patients' => $pagination));
        } else {
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEDADES a where a.codigoe = :codigo GROUP BY a.nombre ORDER BY a.nombre");
            $query->setParameter('codigo', $user->getCodigopal());
            $patients = $query->getResult();

            return $this->render('default/clinicStory.html.twig', array('patients' => $patients));
        }
    }

    /**
     * @Route("/art/informes/siniestros-online", name="sinisterOnline")
     */
    public function sinisterOnlineAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');

        if ($request->isMethod('POST')) {
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEART a where a.codart = :codigo AND a.cuil = :paciente  ORDER BY a.fecha desc , a.enlace asc");
            $query->setParameter('codigo', $user->getCodigopal());
            $query->setParameter('paciente', $request->get('patient'));
            $patients = $query->getResult();
//             $paginator  = $this->get('knp_paginator');
//             $pagination = $paginator->paginate(
//                 $query,
//                 $request->query->get('page', 1)/*page number*/,
//                 10/*limit per page*/
//             );

//             return $this->render('default/sinisterOnlineUser.html.twig', array('patients' => $pagination));
            return $this->render('default/sinisterOnlineUser.html.twig',  array('patients' => $patients));
        } else {
            $query = $oldEm->createQuery("SELECT a FROM OldBundle:iNOVEART a where a.codart = :codigo GROUP BY a.nombre ORDER BY a.cuil asc");
            $query->setParameter('codigo', $user->getCodigopal());
            $patients = $query->getResult();

            return $this->render('default/sinisterOnline.html.twig', array('patients' => $patients));
        }
    }

    /**
     * @Route("/consultas/cuenta-corriente", name="checkingAccount")
     */
    public function checkingAccountAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADAT a WHERE a.codigoe = :codigo ORDER BY a.fecha");
        $query->setParameter('codigo', $user->getCodigopal());
        $datas = $query->getResult();

        return $this->render('default/checkingAccount.html.twig', array('datas' => $datas));
    }

    /**
     * @Route("/art/cuenta-corriente", name="checkingAccount_art")
     */
    public function checkingAccountARTAction(Request $request)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADAT a WHERE a.codigoe = :codigo ORDER BY a.fecha");
        $query->setParameter('codigo',trim( $user->getCodigoe()));
        $datas = $query->getResult();

        return $this->render('default/checkingAccountART.html.twig', array('datas' => $datas));
    }

    /**
     * @Route("/consultas/ver-factura/{nr}", name="seeBill")
     * 
     */
    public function seeBillAction(Request $request, $nr)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADAT a WHERE a.codigoe = :codigo AND a.numero = :numero ORDER BY a.fecha");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('numero', $nr);
        $bill = $query->getResult();
        if (!$bill) {
            throw $this->createNotFoundException(
                'La factura '.$nr.' no existe'
            );
        }
        $bill = array_shift($bill);

        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADET a WHERE a.sucursal = :sucursal AND a.numero = :numero");
        $query->setParameter('sucursal', $bill->getSucursal());
        $query->setParameter('numero', $nr);
        $rows = $query->getResult();

        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iPRESTAWEB a WHERE a.codigoe = :codigo AND a.numero = :numero");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('numero', $nr);
        $services = $query->getResult();

        return $this->render(
            'default/seeBill.html.twig',
            array(
                'bill'      => $bill,
                'rows'      => $rows,
                'services'  => $services,
                'nr'        => $nr
            )
        );
    }
    /**
     * 
     * @Route("/art/ver-factura/{nr}", name="seeBillART")
     */
    public function seeBillARTAction(Request $request, $nr)
    {
    	$user = $this->getUser();
    	$oldEm = $this->get('doctrine')->getManager('old');
    	$query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADAT a WHERE a.codigoe = :codigo AND a.numero = :numero ORDER BY a.fecha");
    	$query->setParameter('codigo', $user->getCodigoe());
    	$query->setParameter('numero', $nr);
    	$bill = $query->getResult();
    	if (!$bill) {
    		throw $this->createNotFoundException(
    				'La factura '.$nr.' no existe'
    				);
    	}
    	$bill = array_shift($bill);
    
    	$query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADET a WHERE a.sucursal = :sucursal AND a.numero = :numero");
    	$query->setParameter('sucursal', $bill->getSucursal());
    	$query->setParameter('numero', $nr);
    	$rows = $query->getResult();
    
    	$query = $oldEm->createQuery("SELECT a FROM OldBundle:iPRESTAWEB a WHERE a.codigoe = :codigo AND a.numero = :numero");
    	$query->setParameter('codigo', $user->getCodigoe());
    	$query->setParameter('numero', $nr);
    	$services = $query->getResult();
    
    	return $this->render(
    			'default/seeBill.html.twig',
    			array(
    					'bill'      => $bill,
    					'rows'      => $rows,
    					'services'  => $services,
    					'nr'        => $nr
    			)
    			);
    }
    /**
     * @Route("/consultas/ver-factura/{nr}/imprimir", name="seeBillPrint")
     */
    public function seeBillActionPrint($nr)
    {
        $user = $this->getUser();
        $oldEm = $this->get('doctrine')->getManager('old');
        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADAT a WHERE a.codigoe = :codigo AND a.numero = :numero ORDER BY a.fecha");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('numero', $nr);
        $bill = $query->getResult();
        if (!$bill) {
            throw $this->createNotFoundException(
                'La factura '.$nr.' no existe'
            );
        }
        $bill = array_shift($bill);

        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iCTADET a WHERE a.sucursal = :sucursal AND a.numero = :numero");
        $query->setParameter('sucursal', $bill->getSucursal());
        $query->setParameter('numero', $nr);
        $rows = $query->getResult();

        $query = $oldEm->createQuery("SELECT a FROM OldBundle:iPRESTAWEB a WHERE a.codigoe = :codigo AND a.numero = :numero");
        $query->setParameter('codigo', $user->getCodigopal());
        $query->setParameter('numero', $nr);
        $services = $query->getResult();

        return $this->render(
            'default/seeBillPrint.html.twig',
            array(
                'bill'      => $bill,
                'rows'      => $rows,
                'services'  => $services,
            )
        );
    }

    /**
     * @Route("/cron/execute", name="cronserver")
     */
    public function cronExecuteAction()
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'import:database',
        ));
        // You can use NullOutput() if you don't need the output
        $output = new StreamOutput(tmpfile(), StreamOutput::VERBOSITY_NORMAL);
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        rewind($output->getStream());
        $content = stream_get_contents($output->getStream());
        fclose($output->getStream());

        // return new Response(""), if you used NullOutput()
        return new Response($content);
    }

    /**
     * @Route("/email/test", name="testemail")
     */
    public function emailTestAction()
    {
        $from = array(
            $this->container->getParameter('email_from') =>
                $this->container->getParameter('name_from')
        );

        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom($from)
            ->setTo('ferchunet@gmail.com')
            ->setBody("<p>este es un mail de prueba!!!</p>",'text/html')
        ;
    
        $value = $this->get('mailer')->send($message);

        var_dump($value);die;
    }

}
