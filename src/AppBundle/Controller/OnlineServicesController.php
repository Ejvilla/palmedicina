<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ServiciosAccidenteTrabajo;
use AppBundle\Entity\ServiciosAtencionConsultorio;
use AppBundle\Entity\ServiciosContactUs;
use AppBundle\Entity\ServiciosEncuestaSatisfaccion;
use AppBundle\Entity\ServiciosExamenMedico;
use AppBundle\Entity\ServiciosVisitaDomicilio;
use AppBundle\Form\ServiciosAccidenteTrabajoType;
use AppBundle\Form\ServiciosAtencionConsultorioType;
use AppBundle\Form\ServiciosContactUsType;
use AppBundle\Form\ServiciosEncuestaSatisfaccionType;
use AppBundle\Form\ServiciosExamenMedicoType;
use AppBundle\Form\ServiciosVisitaDomicilioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OnlineServicesController extends Controller
{
    /**
     * @Route("/consultas/servicios-online/examenes-medicos", name="medicalExaminations")
     */
    public function medicalExaminationsAction(Request $request)
    {
        $examenMedico = new ServiciosExamenMedico();
        $form = $this->createForm(new ServiciosExamenMedicoType(), $examenMedico);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fecha = \DateTime::createFromFormat('d/m/Y', $examenMedico->getFecha());
            $examenMedico->setFecha($fecha);
            $em->persist($examenMedico);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $replyTo = array(
                $examenMedico->getEmail() =>
                    $examenMedico->getApellidoNombreSolicitante()
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Exámenes Médicos')
                ->setFrom($from)
                ->setReplyTo($replyTo)
                ->setTo('consultas@diagnosticolanus.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/medicalExaminations_email.html.twig',
                        array('data' => $examenMedico)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/medicalExaminations.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/servicios-online/atencion-consultorio", name="careClinic")
     */
    public function careClinicAction(Request $request)
    {
        $careClinic = new ServiciosAtencionConsultorio();
        $form = $this->createForm(new ServiciosAtencionConsultorioType(), $careClinic);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fecha = \DateTime::createFromFormat('d/m/Y', $careClinic->getFecha());
            $careClinic->setFecha($fecha);
            $em->persist($careClinic);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $replyTo = array(
                $careClinic->getEmail() =>
                    $careClinic->getApellidoNombreSolicitante()
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Atención en Consultorio')
                ->setFrom($from)
                ->setReplyTo($replyTo)
                ->setTo('consultas@diagnosticolanus.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/careClinic_email.html.twig',
                        array('data' => $careClinic)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/careClinic.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/servicios-online/visita-domicilio", name="visitingAddress")
     */
    public function visitingAddressAction(Request $request)
    {
        $visitingAddress = new ServiciosVisitaDomicilio();
        $form = $this->createForm(new ServiciosVisitaDomicilioType(), $visitingAddress);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fecha = \DateTime::createFromFormat('d/m/Y', $visitingAddress->getFecha());
            $visitingAddress->setFecha($fecha);
            $em->persist($visitingAddress);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $replyTo = array(
                $visitingAddress->getEmail() =>
                    $visitingAddress->getApellidoNombreSolicitante()
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Atención Domiciliaria')
                ->setFrom($from)
                ->setReplyTo($replyTo)
                ->setTo('visitasadomicilio@diagnosticolanus.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/visitingAddress_email.html.twig',
                        array('data' => $visitingAddress)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/visitingAddress.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/servicios-online/accidente-trabajo", name="workAccident")
     */
    public function workAccidentAction(Request $request)
    {
        $workAccident = new ServiciosAccidenteTrabajo();
        $form = $this->createForm(new ServiciosAccidenteTrabajoType(), $workAccident);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fecha = \DateTime::createFromFormat('d/m/Y', $workAccident->getFecha());
            $workAccident->setFecha($fecha);
            $em->persist($workAccident);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $replyTo = array(
                $workAccident->getEmail() =>
                    $workAccident->getApellidoNombreSolicitante()
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Accidente de trabajo')
                ->setFrom($from)
                ->setReplyTo($replyTo)
                ->setTo('art@palmedicinalaboral.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/workAccident_email.html.twig',
                        array('data' => $workAccident)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/workAccident.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/art/servicios-online/accidente-trabajo", name="workAccident_art")
     */
    public function workAccidentARTAction(Request $request)
    {
        $workAccident = new ServiciosAccidenteTrabajo();
        $form = $this->createForm(new ServiciosAccidenteTrabajoType(), $workAccident);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $fecha = \DateTime::createFromFormat('d/m/Y', $workAccident->getFecha());
            $workAccident->setFecha($fecha);
            $em->persist($workAccident);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $replyTo = array(
                $workAccident->getEmail() =>
                    $workAccident->getApellidoNombreSolicitante()
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Accidente de trabajo')
                ->setFrom($from)
                ->setReplyTo($replyTo)
                ->setTo('art@palmedicinalaboral.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/workAccident_email.html.twig',
                        array('data' => $workAccident)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/workAccident.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/servicios-online/encuesta-satisfaccion", name="satisfactionSurvey")
     */
    public function satisfactionSurveyAction(Request $request)
    {
        $satisfactionSurvey = new ServiciosEncuestaSatisfaccion();
        $form = $this->createForm(new ServiciosEncuestaSatisfaccionType(), $satisfactionSurvey);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($satisfactionSurvey);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Encuesta de Satisfacción')
                ->setFrom($from)
                ->setTo('rrhh@diagnosticolanus.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/satisfactionSurvey_email.html.twig',
                        array('data' => $satisfactionSurvey)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/satisfactionSurvey.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/art/servicios-online/encuesta-satisfaccion", name="satisfactionSurvey_art")
     */
    public function satisfactionSurveyARTAction(Request $request)
    {
        $satisfactionSurvey = new ServiciosEncuestaSatisfaccion();
        $form = $this->createForm(new ServiciosEncuestaSatisfaccionType(), $satisfactionSurvey);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($satisfactionSurvey);
            $em->flush();

            $from = array(
                $this->container->getParameter('email_from') =>
                    $this->container->getParameter('name_from')
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Formulario - Encuesta de Satisfacción')
                ->setFrom($from)
                ->setTo('rrhh@diagnosticolanus.com.ar')
                ->setBody(
                    $this->renderView(
                        'OnlineServices/satisfactionSurvey_email.html.twig',
                        array('data' => $satisfactionSurvey)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/satisfactionSurvey.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/ayuda/consultas", name="contactUs")
     */
    public function contactUsAction(Request $request)
    {
        $contactUs = new ServiciosContactUs();
        $form = $this->createForm(new ServiciosContactUsType(), $contactUs);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactUs);
            $em->flush();
            return $this->redirect($this->generateUrl('formSuccess'));
        }

        return $this->render(
            'OnlineServices/contactUs.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/art/ayuda/contactenos", name="contactUs_art")
     */
    public function contactUsARTAction(Request $request)
    {
        $contactUs = new ServiciosContactUs();
        $form = $this->createForm(new ServiciosContactUsType(), $contactUs);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactUs);
            $em->flush();
            return $this->redirect($this->generateUrl('formSuccess_art'));
        }

        return $this->render(
            'OnlineServices/contactUs.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/consultas/servicios-online/formulario-enviado", name="formSuccess")
     * @Route("/art/servicios-online/formulario-enviado", name="formSuccess_art")
     */
    public function formSuccessAction()
    {
        return $this->render('OnlineServices/formSuccess.html.twig');
    }
}
